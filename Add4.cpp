﻿
#include <iostream>
#define N 6

int main()
{
	int lot = 1;
	int mas[N];

	for (int i = 100000; i < 1000000; i++)
	{
		int n = i;
		for (int j = 0; j < 6; j++)
		{
			mas[j] = n % 10;
			n /= 10;
		}

		if ((mas[0] + mas[1] + mas[2]) == (mas[3] + mas[4] + mas[5]))
				lot += 1;
	}
	std::cout << lot;
	return 0;
}
